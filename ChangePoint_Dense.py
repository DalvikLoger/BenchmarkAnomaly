import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
from sklearn.cluster import KMeans
sns.set_theme() # pour modifier le thème
from sklearn import svm
from sklearn.datasets import make_moons, make_blobs
from sklearn.ensemble import IsolationForest
from sklearn.model_selection import train_test_split, GridSearchCV, StratifiedKFold
from sklearn.metrics import make_scorer, classification_report, make_scorer, recall_score, f1_score
from sklearn.preprocessing import MinMaxScaler

db = pd.read_csv('anomaly-free/anomaly-free.csv',header=0, sep=';' )
df0 = pd.read_csv('valve1/0.csv',header=0, sep=';' )
df1 = pd.read_csv('valve1/1.csv',header=0, sep=';' )
df2 = pd.read_csv('valve1/2.csv',header=0, sep=';' )
df3 = pd.read_csv('valve1/3.csv',header=0, sep=';' )
df4 = pd.read_csv('valve1/4.csv',header=0, sep=';' )
df5 = pd.read_csv('valve1/5.csv',header=0, sep=';' )
df6 = pd.read_csv('valve1/6.csv',header=0, sep=';' )
df7 = pd.read_csv('valve1/7.csv',header=0, sep=';' )
df8 = pd.read_csv('valve1/8.csv',header=0, sep=';' )
df9 = pd.read_csv('valve1/9.csv',header=0, sep=';' )
df10 = pd.read_csv('valve1/10.csv',header=0, sep=';' )

df = pd.concat([df0,df1,df2,df3,df4,df5,df6,df7,df8,df9,df10], axis = 0)

df = df.drop('datetime', axis=1)
data = df.drop('changepoint', axis=1)
target = df.changepoint
X_train, X_test, y_train, y_test= train_test_split(data, target, test_size=0.2)

from tensorflow.keras.layers import Input, Dense #Pour instancier une couche Dense et une d'Input
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.models import Model

inputs = Input(shape = (9), name = "Input")

dense1 = Dense(units = 10, activation = "tanh", name = "Dense_1")
dense2 = Dense(units = 8, activation = "tanh", name = "Dense_2")
dense3 = Dense(units = 6, activation = "tanh", name = "Dense_3")
dense4 = Dense(units = 2, activation = "softmax", name = "Dense_4")

x=dense1(inputs)
x=dense2(x)
x=dense3(x)
outputs=dense4(x)

model = Model(inputs = inputs, outputs = outputs)
model.summary()

model.compile(loss = "sparse_categorical_crossentropy",
              optimizer = "adam",
              metrics = ["accuracy"])
model.fit(X_train,y_train,epochs=3,batch_size=32,validation_split=0.1)

y_pred = model.predict(X_test)

test_pred = model.predict(X_test)

y_test_class = y_test
y_pred_class = np.argmax(test_pred,axis=1)
from sklearn.metrics import classification_report,confusion_matrix
print(classification_report(y_test_class,y_pred_class))
print(confusion_matrix(y_test_class,y_pred_class))

