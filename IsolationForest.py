import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
from sklearn.cluster import KMeans
sns.set_theme() # pour modifier le thème
from sklearn import svm
from sklearn.datasets import make_moons, make_blobs
from sklearn.ensemble import IsolationForest
from sklearn.model_selection import train_test_split, GridSearchCV, StratifiedKFold
from sklearn.metrics import make_scorer, classification_report, make_scorer, recall_score, f1_score
from sklearn.preprocessing import MinMaxScaler

db = pd.read_csv('anomaly-free/anomaly-free.csv',header=0, sep=';' )
df0 = pd.read_csv('valve1/0.csv',header=0, sep=';' )
df1 = pd.read_csv('valve1/1.csv',header=0, sep=';' )
df2 = pd.read_csv('valve1/2.csv',header=0, sep=';' )
df3 = pd.read_csv('valve1/3.csv',header=0, sep=';' )
df4 = pd.read_csv('valve1/4.csv',header=0, sep=';' )
df5 = pd.read_csv('valve1/5.csv',header=0, sep=';' )
df6 = pd.read_csv('valve1/6.csv',header=0, sep=';' )
df7 = pd.read_csv('valve1/7.csv',header=0, sep=';' )
df8 = pd.read_csv('valve1/8.csv',header=0, sep=';' )
df9 = pd.read_csv('valve1/9.csv',header=0, sep=';' )
df10 = pd.read_csv('valve1/10.csv',header=0, sep=';' )

df = pd.concat([df0,df1,df2,df3,df4,df5,df6,df7,df8,df9,df10], axis = 0)

X_train, X_test, y_train, y_test= train_test_split(df, df.anomaly, test_size=0.2)
X_train = X_train.drop('anomaly', axis = 1)
X_test = X_test.drop('anomaly', axis = 1)
X_train = X_train.drop('datetime', axis=1)
X_test = X_test.drop('datetime', axis=1)

isof = IsolationForest(contamination=0.03, n_estimators=100, n_jobs=-1)
isof.fit(X_train)

y_pred = isof.predict(X_test)
print(classification_report(y_test, y_pred))
pd.crosstab(y_test, y_pred, rownames=['Classes réelles'], colnames=['Classes prédites'])

