import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
from sklearn.cluster import KMeans
sns.set_theme() # pour modifier le thème
from sklearn import svm
from sklearn.datasets import make_moons, make_blobs
from sklearn.ensemble import IsolationForest
from sklearn.model_selection import train_test_split, GridSearchCV, StratifiedKFold
from sklearn.metrics import make_scorer, classification_report, make_scorer, recall_score, f1_score
from sklearn.preprocessing import MinMaxScaler

db = pd.read_csv('anomaly-free/anomaly-free.csv',header=0, sep=';' )
df0 = pd.read_csv('valve1/0.csv',header=0, sep=';' )
df1 = pd.read_csv('valve1/1.csv',header=0, sep=';' )
df2 = pd.read_csv('valve1/2.csv',header=0, sep=';' )
df3 = pd.read_csv('valve1/3.csv',header=0, sep=';' )
df4 = pd.read_csv('valve1/4.csv',header=0, sep=';' )
df5 = pd.read_csv('valve1/5.csv',header=0, sep=';' )
df6 = pd.read_csv('valve1/6.csv',header=0, sep=';' )
df7 = pd.read_csv('valve1/7.csv',header=0, sep=';' )
df8 = pd.read_csv('valve1/8.csv',header=0, sep=';' )
df9 = pd.read_csv('valve1/9.csv',header=0, sep=';' )
df10 = pd.read_csv('valve1/10.csv',header=0, sep=';' )

df = pd.concat([df0,df1,df2,df3,df4,df5,df6,df7,df8,df9,df10], axis = 0)

df['changepoint'] = df['changepoint'].replace(to_replace=[0,1],value=[-1,1])

df = df.drop('datetime', axis=1)
data = df.drop('changepoint', axis=1)

from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Conv1D, LSTM, RepeatVector, TimeDistributed, Flatten, Dense
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense
from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler()
X = scaler.fit_transform(data)
X_train = X.reshape(X.shape[0], 1, X.shape[1])


def build_mscred_model(input_shape):
    # Paramètres du modèle
    filters = 64
    kernel_size = 3
    lstm_units = 32

    # Définition de l'entrée
    input_layer = Input(shape=(input_shape[0], input_shape[1]))

    # Encoder
    encoder = Conv1D(filters, kernel_size, activation='relu', padding='same')(input_layer)
    encoder = LSTM(lstm_units, activation='relu', return_sequences=True)(encoder)

    # Bottleneck
    bottleneck = LSTM(lstm_units, activation='relu', return_sequences=False)(encoder)

    # RepeatVector pour transformer le bottleneck en séquence
    repeat_vector = RepeatVector(input_shape[0])(bottleneck)

    # Decoder
    decoder = LSTM(lstm_units, activation='relu', return_sequences=True)(repeat_vector)
    decoder = Conv1D(filters, kernel_size, activation='relu', padding='same')(decoder)

    # TimeDistributed Dense pour chaque étape temporelle
    output_layer = TimeDistributed(Dense(input_shape[1]))(decoder)

    # Modèle complet
    model = Model(inputs=input_layer, outputs=output_layer)

    return model

# Exemple d'utilisation
input_shape = (1,X.shape[1] )  # Remplacez par la forme de vos données
mscred_model = build_mscred_model(input_shape)
mscred_model.summary()

mscred_model.compile(optimizer='adam', loss='mae')
nb_epochs = 15
batch_size = 10
history = mscred_model.fit(X_train, X_train, epochs=nb_epochs, batch_size=batch_size, validation_split=0.05).history

X_pred = mscred_model.predict(X_train)
X_pred = X_pred.reshape(X_pred.shape[0], X_pred.shape[2])
X_pred = pd.DataFrame(X_pred, columns=data.columns)
X_pred.index = data.index

scored = pd.DataFrame(index=data.index)
X_train1 = X_train.reshape(X_train.shape[0], X_train.shape[2])
scored['Loss_mae'] = np.mean(np.abs(X_pred-X_train1), axis = 1)
scored['Threshold'] = 0.11
scored['Anomaly'] = scored['Loss_mae'] > scored['Threshold']

anomalies = scored[scored.Anomaly == True]
anomalies.head(20)