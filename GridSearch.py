import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
from sklearn.cluster import KMeans
sns.set_theme() # pour modifier le thème
from sklearn import svm
from sklearn.datasets import make_moons, make_blobs
from sklearn.ensemble import IsolationForest
from sklearn.model_selection import train_test_split, GridSearchCV, StratifiedKFold
from sklearn.metrics import make_scorer, classification_report, make_scorer, recall_score, f1_score
from sklearn.preprocessing import MinMaxScaler

db = pd.read_csv('anomaly-free/anomaly-free.csv',header=0, sep=';' )
df0 = pd.read_csv('valve1/0.csv',header=0, sep=';' )
df1 = pd.read_csv('valve1/1.csv',header=0, sep=';' )
df2 = pd.read_csv('valve1/2.csv',header=0, sep=';' )
df3 = pd.read_csv('valve1/3.csv',header=0, sep=';' )
df4 = pd.read_csv('valve1/4.csv',header=0, sep=';' )
df5 = pd.read_csv('valve1/5.csv',header=0, sep=';' )
df6 = pd.read_csv('valve1/6.csv',header=0, sep=';' )
df7 = pd.read_csv('valve1/7.csv',header=0, sep=';' )
df8 = pd.read_csv('valve1/8.csv',header=0, sep=';' )
df9 = pd.read_csv('valve1/9.csv',header=0, sep=';' )
df10 = pd.read_csv('valve1/10.csv',header=0, sep=';' )

df = pd.concat([df0,df1,df2,df3,df4,df5,df6,df7,df8,df9,df10], axis = 0)

X_train, X_test, y_train, y_test= train_test_split(df, df.anomaly, test_size=0.2)
X_train = X_train.drop('anomaly', axis = 1)
X_test = X_test.drop('anomaly', axis = 1)
X_train = X_train.drop('datetime', axis=1)
X_test = X_test.drop('datetime', axis=1)

skf = StratifiedKFold(n_splits=3) 
folds = list(skf.split(X_train, y_train))
forest = IsolationForest()

# Dans la situation où on ne connait pas à priori le paramètre de contamination,
# on ajoutera la contamination dans la grille de recherche

resc = make_scorer(recall_score,pos_label=-1)

params = {'contamination': np.linspace(0.01, 0.05, 10), 'n_estimators': [100,200,300]}

search = GridSearchCV(estimator=forest, param_grid=params, scoring=resc, cv=folds, n_jobs=-1)
search.fit(X_train, y_train)

# predict
optimal_forest = search.best_estimator_
y_pred = optimal_forest.predict(X_test)

print(classification_report(y_test, y_pred))

pd.crosstab(y_test, y_pred, rownames=['Classes réelles'], colnames=['Classes prédites'])

